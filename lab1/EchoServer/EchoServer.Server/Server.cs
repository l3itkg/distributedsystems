﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EchoServer.Server
{
    public class Server : IDisposable
    {
        private TcpListener _server;
        private Task _serverThread = null;
        private Task _purgingThread = null;
        private readonly ClientsCollection _clients = null;
        private const int MaxClients = 10;
        private readonly CancellationTokenSource _cancellation;

        public Server()
        {
            _clients = new ClientsCollection();
            _cancellation=new CancellationTokenSource();
        }

        public Server Init()
        {
            return Init(new Helper().DefaultEndPoint);
        }

        public Server Init(IPEndPoint ipNport)
        {
            try
            {
                _server = new TcpListener(ipNport);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                _server = null;
                Environment.Exit(1);
            }

            return this;
        }

        public Task StartServer()
        {
            _server.Start();
            _serverThread = new Task(ServerThreadStart, TaskCreationOptions.DenyChildAttach);    
            _serverThread.Start();

            _purgingThread = new Task(PurgingThreadStart);
            _purgingThread.Start();
            return _purgingThread;
        }

        private void ServerThreadStart()
        {
            while (true)
            {
                try
                {
                    var client = _server.AcceptTcpClient();
                    lock (_clients)
                    {
                        if (_clients.Count() > MaxClients)
                        {
                            try
                            {
                                var sorry = Encoding.UTF8.GetBytes("Server is overloaded. Sorry.");
                                client.GetStream().Write(sorry, 0, sorry.Length);
                                client.Dispose();
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e);
                                throw;
                            }
                        }
                    }
                    var handler = new ClientHandler(client,_clients.SendAll, _cancellation.Token);
                    lock (_clients)
                    {
                        _clients.AddClient(handler);
                        new Task(handler.Serve).Start();
                    }
                }
                catch (SocketException ex)
                {
                    Console.WriteLine(ex);
                    Environment.Exit(1);
                }
            }
        }

        public void ShutDown()
        {
            _cancellation?.Cancel();
            _purgingThread?.Wait();
        }

        private void PurgingThreadStart()
        {
            while (true)
            {
                List<ClientHandler> deleteList = new List<ClientHandler>();
                if (_cancellation.IsCancellationRequested)
                {
                    lock (_clients)
                    {
                        _clients.Dispose();
                    }

                    return;
                }
                lock (_clients)
                {
                    _clients.CleanUp();
                }
                Thread.Sleep(3000);
            }
        }

        public void Dispose()
        {
            ShutDown();
        }
    }
}
