﻿using System;
using System.Collections.Generic;

namespace EchoServer.Server
{
    public class ClientsCollection:IDisposable
    {
        private List<ClientHandler> _clients;

        public ClientsCollection()
        {
            this._clients = new List<ClientHandler>();
        }

        public void SendAll(string message)
        {
            lock (_clients)
            {
                foreach (var clientHandler in _clients)
                {
                    clientHandler.SendMessage(message);
                }
            }
        }

        public void AddClient(ClientHandler handler)
        {
            lock (_clients)
            {
                _clients.Add(handler);
            }
        }

        public int Count()
        {
            lock (_clients)
            {
                return _clients.Count;
            }
        }

        public void CleanUp()
        {
            var deleteList = new List<ClientHandler>();
            lock (_clients)
            {
                foreach (var client in _clients)
                {
                    if (client.IsMarkedForDeletion())
                    {
                        deleteList.Add(client);
                        client.StopSocketListener();
                        client.Dispose();
                    }
                }
                foreach (var client in deleteList)
                {
                    _clients.Remove(client);
                }
            }
        }

        public void Dispose()
        {
            foreach (var clientHandler in _clients)
            {
                clientHandler.Dispose();
            }
        }
    }
}