﻿using System;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EchoServer.Server
{
    public class ClientHandler : IDisposable
    {
        private readonly TcpClient _client;

        private bool _markedForDeletion;
        private StringBuilder _buffer = new StringBuilder();
        private ClientMessageBuffer _messageQueue;
        private CancellationToken _stopRequested;
        private readonly Action<string> _sendAll;

        /// <summary>
        /// token - для остановки работы handler
        /// </summary>
        /// <param name="tcpClient"></param>
        /// <param name="sendAll"></param>
        /// <param name="token"></param>
        public ClientHandler(TcpClient tcpClient, Action<string> sendAll, CancellationToken token) : base()
        {
            _client = tcpClient ?? throw new Exception("tcpClient is null ");
            _markedForDeletion = false;
            _sendAll = sendAll;
            _messageQueue =new ClientMessageBuffer();
            _stopRequested = token;
        }

        public void Serve()
        {
            new Task(Speak).Start();
            new Task(Listen).Start();
        }
        
        public void Listen()
        {
            try
            {
                if (_stopRequested.IsCancellationRequested) return;
                var stream = _client.GetStream();

                var reader = new StreamReader(stream);
                while (true)
                {
                    if (_stopRequested.IsCancellationRequested) return;
                    if (!_client.Connected)
                    {
                        _markedForDeletion = true;
                        return;
                    }

                    ;
                    if (!stream.DataAvailable)
                    {
                        Thread.Sleep(0);
                        continue;
                    }
                    string message = reader.ReadLine();
                    if (string.IsNullOrEmpty(message)) continue;
                    var splitted = message.Split('\n').Where(x => !string.IsNullOrWhiteSpace(x)).ToArray();
                    foreach (var s in splitted)
                    {
                        _sendAll(s);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {

                _markedForDeletion = true;
            }
        }

        /// <summary>
        /// cancellation на случай, если нужно остановить работу потока
        /// </summary>
        /// <param name="cancellation"></param>
        private void Speak()
        {
            var stream = _client.GetStream();
            var writer = new StreamWriter(stream){AutoFlush = true};
            while (!_stopRequested.IsCancellationRequested)
            {

                var message = _messageQueue.Pop();
                if (!_client.Connected)
                {
                    _markedForDeletion = true;
                    return;
                };
                if (message == null)
                {
                    Thread.Sleep(0);
                    continue;
                }
                writer.Write(message+"\n");
            }
        }

        public void SendMessage(string message)
        {
            _messageQueue.Push(message);
        }

        public void StopSocketListener()
        {
            _client?.Close();
        }

        public bool IsMarkedForDeletion()
        {
            return _markedForDeletion;
        }

        public void Dispose()
        {
            var exit = Encoding.UTF8.GetBytes("#exit");
            _client?.GetStream()?.Write(exit, 0, exit.Length);
            _client?.Dispose();
        }
    }
}