﻿using System.Collections.Concurrent;
using System.Threading;

namespace EchoServer.Server
{
    public class ClientMessageBuffer
    {
        private readonly int _queueSize;
        private ConcurrentQueue<string> Queue { get; set; }
        private readonly SemaphoreSlim _full;
        public ClientMessageBuffer( int queueSize=50)
        {
            _queueSize = queueSize;
            Queue = new ConcurrentQueue<string>();
            _full = new SemaphoreSlim(0, _queueSize);
        }


        public void Push(string message)
        {
            if (Queue.Count >= _queueSize) return;
            Queue.Enqueue(message);
            _full.Release(1);
        }

        /// <summary>
        /// Пытаемся взять сообщение. Если за 200 мс не получилось, то вернет null.
        /// </summary>
        /// <returns></returns>
        public string Pop()
        {
            if (!_full.Wait(200)) return null;
            if (Queue.TryDequeue(out var result)) return result;
            return null;
        }
    }
}