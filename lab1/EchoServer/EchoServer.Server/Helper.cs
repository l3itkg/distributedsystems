﻿using System.Net;

namespace EchoServer.Server
{
    public class Helper
    {
        public static IPAddress Server = IPAddress.Parse("127.0.0.1");
        public static int DefaultPort = 10001;
        public IPEndPoint DefaultEndPoint;

        public Helper()
        {
            DefaultEndPoint = new IPEndPoint(Server, DefaultPort);
        }

        public void StartSocketListener()
        {

        }
    }
}
