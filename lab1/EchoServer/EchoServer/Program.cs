﻿using System;
using System.Diagnostics;

namespace EchoServer
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = Process.GetCurrentProcess().Id.ToString();
            var server = new Server.Server();
            var task = server.Init().StartServer();
            Console.WriteLine("Write 'exit' to stop");
            while (Console.ReadLine() != "exit")
            {

            }

            server.ShutDown();
        }
    }
}
