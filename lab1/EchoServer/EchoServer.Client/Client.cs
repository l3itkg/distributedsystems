﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EchoServer.ClientApp
{
    public class Client : IDisposable
    {
        public string ServerIp { get; }
        public int ServerPort { get; }
        public Action<string> OutputAction { private get; set; }
        public event Action<string> MessageReceived;
        private TcpClient Socket { get; set; }
        public string ClientId { get; private set; }
        private Task listTask { get; set; }
        private CancellationTokenSource CancellationTokenSource = new CancellationTokenSource();

        public Client(string ip, int port)
        {
            ServerIp = ip;
            ServerPort = port;
            ClientId = Guid.NewGuid().ToString();
        } 

        public void Connect()
        {
            try
            {
                TcpClient client = new TcpClient();
                OutputAction($"Connecting to {ServerIp}:{ServerPort}");
                client.Connect(ServerIp, ServerPort);
                OutputAction($"Connected to {ServerIp}:{ServerPort}");
                Socket = client;
            }
            catch (SocketException e)
            {
                OutputAction($"Connection interrupted due to exception: " + e);
                throw;
            }
        }

        public async Task Write(string message)
        {
            try
            {
                var msg = Encoding.UTF8.GetBytes(message + "\n");
                await Socket.GetStream().WriteAsync(msg, 0, msg.Length);
                
            }
            catch (Exception e)
            {
                OutputAction("Error occured while sending : " + e);
            }
        }

        public void Start()
        {
            listTask = new Task(Listen);
            listTask.Start();
        }



        public void Listen()
        {
            try
            {

                if (MessageReceived == null)
                {
                    throw new Exception("MessageReceived event -> nobody subscribed");
                }
                var stream = Socket.GetStream();
                var reader = new StreamReader(stream);
                while (true)
                {
                    if (CancellationTokenSource.IsCancellationRequested) return;
                    if (!Socket.GetStream().DataAvailable)
                    {
                        Thread.Sleep(0);
                        continue;
                    }

                    string message = reader.ReadLine();
                    if (message == "#exit")
                    {
                        MessageReceived("Server Shutdown");
                        Thread.Sleep(3000);
                        Environment.Exit(32);
                    }
                    MessageReceived(message);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public void Dispose()
        {
            CancellationTokenSource.Cancel();
            listTask.Wait();
            Socket?.Dispose();
        }

    }
}
