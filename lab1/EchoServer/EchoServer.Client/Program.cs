using System;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
namespace EchoServer.ClientApp
{
    class Program
    {
        private static Client _client;
        static void Main(string[] args)
        {
            Console.Title = Process.GetCurrentProcess().Id.ToString();
            Console.WriteLine("Welcome to the Client App");
            Console.WriteLine("Please, enter the ip and port of the server or press ENTER to use \"127.0.0.1:10001\" :");
            var address = string.Empty;
            while (true)
            {
                address = Console.ReadLine();
                if (string.IsNullOrEmpty(address)) address = "127.0.0.1:10001";
                if (Regex.IsMatch(address, "^[1-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}:[1-9][0-9]{1,4}$"))
                {
                    break;
                }
                else Console.WriteLine($"The address entered \"{address}\" is not valid");
            }

            var serverAddress = address.Split(':');
            _client = new ClientApp.Client(serverAddress[0], int.Parse(serverAddress[1])) {OutputAction = Console.WriteLine};
            _client.Connect();
            _client.MessageReceived += Console.WriteLine;
            _client.MessageReceived += WriteIntoFile;
            string message;
            Console.WriteLine("To exit type '#exit'");
            _client.Start();
            while ((message=Console.ReadLine())!="#exit")
            {
                _client.Write(message).Wait();
            }
            Console.WriteLine("Shutting down");
            _client.Dispose();
        }

        private static void WriteIntoFile(string message)
        {
            var path = Path.Combine(Environment.CurrentDirectory,"~","files");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            File.AppendAllText(Path.Combine(path, _client.ClientId + ".txt"), message+Environment.NewLine);
        }
    }
}
